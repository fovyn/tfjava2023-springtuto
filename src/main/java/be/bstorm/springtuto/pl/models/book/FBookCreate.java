package be.bstorm.springtuto.pl.models.book;

import be.bstorm.springtuto.dal.entities.BookEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FBookCreate {
    private String title;
    private String isbn;

    public BookEntity toEntity() {
        BookEntity entity = new BookEntity();

        entity.setIsbn(isbn);
        entity.setTitle(title);

        return entity;
    }
}

package be.bstorm.springtuto.pl.models.book;

import be.bstorm.springtuto.dal.entities.BookEntity;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
public class BookReadDTO {
    private String isbn;
    private String title;
    @Singular
    private List<String> authors;

    public static BookReadDTO fromEntity(BookEntity entity) {
        return BookReadDTO.builder()
                .isbn(entity.getIsbn())
                .title(entity.getTitle())
                .build();
    }
}

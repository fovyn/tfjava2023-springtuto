package be.bstorm.springtuto.pl.controllers;

import be.bstorm.springtuto.bll.author.AuthorService;
import be.bstorm.springtuto.bll.book.BookService;
import be.bstorm.springtuto.dal.entities.AuthorEntity;
import be.bstorm.springtuto.dal.entities.BookEntity;
import be.bstorm.springtuto.pl.models.book.BookReadDTO;
import be.bstorm.springtuto.pl.models.book.FBookCreate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = {"/books"})
public class BookController {

    private final BookService bookService;
    private final AuthorService authorService;

    public BookController(BookService bookService, AuthorService authorService) {
        this.bookService = bookService;
        this.authorService = authorService;
    }

    // GET http://localhost:8080/books
    @GetMapping
    public List<BookEntity> readAllAction() {
        return this.bookService.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public BookReadDTO readOneAction(
            @PathVariable("id") Long id
    ) {
        BookReadDTO dto = this.bookService.findOneById(id).map(BookReadDTO::fromEntity).orElseThrow();
        List<AuthorEntity> authors = this.authorService.findAllByBookId(id);
        dto.setAuthors(authors.stream().map(it -> it.getLastname()+ "-"+ it.getFirstname()).toList());
        return dto;
    }

    @GetMapping(path = {"/{id}/authors"})
    public List<AuthorEntity> readAuthorFromBookId(
            @PathVariable("id") Long id
    ) {
        return this.authorService.findAllByBookId(id);
    }

    // POST http://localhost:8080/books
    @PostMapping
    public BookEntity createAction(
            @RequestBody FBookCreate form
    ) {
        BookEntity entity = form.toEntity();

        return this.bookService.create(entity);
    }
}

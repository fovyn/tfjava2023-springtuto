package be.bstorm.springtuto.bll.write;

import be.bstorm.springtuto.bll.CrudService;
import be.bstorm.springtuto.dal.entities.WriteRelation;

public interface WriteService extends CrudService<WriteRelation, WriteRelation.WriteId> {
}

package be.bstorm.springtuto.bll.write;

import be.bstorm.springtuto.dal.entities.WriteRelation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WriteServiceImpl implements WriteService {

    @Override
    public List<WriteRelation> findAll() {
        return null;
    }

    @Override
    public Optional<WriteRelation> findOneById(WriteRelation.WriteId id) {
        return Optional.empty();
    }

    @Override
    public WriteRelation create(WriteRelation writeRelation) {
        return null;
    }

    @Override
    public WriteRelation update(WriteRelation.WriteId id, WriteRelation writeRelation) {
        return null;
    }

    @Override
    public void delete(WriteRelation.WriteId id) {

    }
}

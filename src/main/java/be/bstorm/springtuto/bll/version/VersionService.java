package be.bstorm.springtuto.bll.version;

import be.bstorm.springtuto.bll.CrudService;
import be.bstorm.springtuto.dal.entities.VersionEntity;

public interface VersionService extends CrudService<VersionEntity, Long> {
}

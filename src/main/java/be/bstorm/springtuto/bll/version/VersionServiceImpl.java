package be.bstorm.springtuto.bll.version;

import be.bstorm.springtuto.dal.entities.VersionEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VersionServiceImpl implements VersionService {
    @Override
    public List<VersionEntity> findAll() {
        return null;
    }

    @Override
    public Optional<VersionEntity> findOneById(Long id) {
        return Optional.empty();
    }

    @Override
    public VersionEntity create(VersionEntity versionEntity) {
        return null;
    }

    @Override
    public VersionEntity update(Long id, VersionEntity versionEntity) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}

package be.bstorm.springtuto.bll.bookStore;

import be.bstorm.springtuto.dal.entities.BookStoreEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookStoreServiceImpl implements BookStoreService {
    @Override
    public List<BookStoreEntity> findAll() {
        return null;
    }

    @Override
    public Optional<BookStoreEntity> findOneById(Long id) {
        return Optional.empty();
    }

    @Override
    public BookStoreEntity create(BookStoreEntity bookStoreEntity) {
        return null;
    }

    @Override
    public BookStoreEntity update(Long id, BookStoreEntity bookStoreEntity) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}

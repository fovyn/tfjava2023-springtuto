package be.bstorm.springtuto.bll.bookStore;

import be.bstorm.springtuto.bll.CrudService;
import be.bstorm.springtuto.dal.entities.BookStoreEntity;

public interface BookStoreService extends CrudService<BookStoreEntity, Long> {
}

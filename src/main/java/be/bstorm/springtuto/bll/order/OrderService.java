package be.bstorm.springtuto.bll.order;

import be.bstorm.springtuto.bll.CrudService;
import be.bstorm.springtuto.dal.entities.OrderRelation;

public interface OrderService extends CrudService<OrderRelation, OrderRelation.OrderId> {
}

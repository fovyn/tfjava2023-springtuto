package be.bstorm.springtuto.bll.order;

import be.bstorm.springtuto.dal.entities.OrderRelation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {
    @Override
    public List<OrderRelation> findAll() {
        return null;
    }

    @Override
    public Optional<OrderRelation> findOneById(OrderRelation.OrderId id) {
        return Optional.empty();
    }

    @Override
    public OrderRelation create(OrderRelation orderRelation) {
        return null;
    }

    @Override
    public OrderRelation update(OrderRelation.OrderId id, OrderRelation orderRelation) {
        return null;
    }

    @Override
    public void delete(OrderRelation.OrderId id) {

    }
}

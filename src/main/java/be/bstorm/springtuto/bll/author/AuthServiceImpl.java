package be.bstorm.springtuto.bll.author;

import be.bstorm.springtuto.dal.entities.AuthorEntity;
import be.bstorm.springtuto.dal.repositories.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<AuthorEntity> findAll() {
        return null;
    }

    @Override
    public Optional<AuthorEntity> findOneById(Long id) {
        return Optional.empty();
    }

    @Override
    public AuthorEntity create(AuthorEntity authorEntity) {
        return null;
    }

    @Override
    public AuthorEntity update(Long id, AuthorEntity authorEntity) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<AuthorEntity> findAllByBookId(Long id) {
        return this.authorRepository.findAllByBookId(id);
    }
}

package be.bstorm.springtuto.bll.author;

import be.bstorm.springtuto.bll.CrudService;
import be.bstorm.springtuto.dal.entities.AuthorEntity;

import java.util.List;

public interface AuthorService extends CrudService<AuthorEntity, Long> {

    List<AuthorEntity> findAllByBookId(Long id);
}

package be.bstorm.springtuto.bll;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface CrudService<TModel, TKey extends Serializable> {
    List<TModel> findAll();
    Optional<TModel> findOneById(TKey id);
    TModel create(TModel model);
    TModel update(TKey id, TModel model);
    void delete(TKey id);
}

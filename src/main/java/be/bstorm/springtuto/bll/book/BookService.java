package be.bstorm.springtuto.bll.book;

import be.bstorm.springtuto.bll.CrudService;
import be.bstorm.springtuto.dal.entities.BookEntity;

public interface BookService extends CrudService<BookEntity, Long> {
}

package be.bstorm.springtuto.bll.book;

import be.bstorm.springtuto.dal.entities.BookEntity;
import be.bstorm.springtuto.dal.repositories.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookEntity> findAll() {
        return this.bookRepository.findAllActive();
    }

    @Override
    public Optional<BookEntity> findOneById(Long id) {
        return this.bookRepository.findById(id);
    }

    @Override
    public BookEntity create(BookEntity bookEntity) {
        return this.bookRepository.save(bookEntity);
    }

    @Override
    public BookEntity update(Long id, BookEntity bookEntity) {
        BookEntity toUpdate = this.findOneById(id).orElseThrow();

        if (bookEntity.getIsbn() != null) {
            toUpdate.setIsbn(bookEntity.getIsbn());
        }
        if (bookEntity.getTitle() != null) {
            toUpdate.setTitle(bookEntity.getTitle());
        }

        toUpdate.setActive(bookEntity.isActive());

        return this.bookRepository.save(toUpdate);
    }

    @Override
    public void delete(Long id) {
        BookEntity toDelete = this.findOneById(id).orElseThrow();
        toDelete.setActive(false);

        this.bookRepository.save(toDelete);
    }
}

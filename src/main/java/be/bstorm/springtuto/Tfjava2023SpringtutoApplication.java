package be.bstorm.springtuto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Tfjava2023SpringtutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tfjava2023SpringtutoApplication.class, args);
	}


}

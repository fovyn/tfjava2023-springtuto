package be.bstorm.springtuto.dal.repositories;

import be.bstorm.springtuto.dal.entities.VersionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VersionRepository extends JpaRepository<VersionEntity, Long> {
}

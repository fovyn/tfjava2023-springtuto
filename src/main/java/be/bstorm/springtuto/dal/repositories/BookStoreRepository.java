package be.bstorm.springtuto.dal.repositories;

import be.bstorm.springtuto.dal.entities.BookStoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookStoreRepository extends JpaRepository<BookStoreEntity, Long> {
}

package be.bstorm.springtuto.dal.repositories;

import be.bstorm.springtuto.dal.entities.WriteRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WriteRepository extends JpaRepository<WriteRelation, WriteRelation.WriteId> {
}

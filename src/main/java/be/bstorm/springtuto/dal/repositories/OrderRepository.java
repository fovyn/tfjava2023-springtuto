package be.bstorm.springtuto.dal.repositories;

import be.bstorm.springtuto.dal.entities.OrderRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<OrderRelation, OrderRelation.OrderId> {
}

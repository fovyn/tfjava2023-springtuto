package be.bstorm.springtuto.dal.repositories;

import be.bstorm.springtuto.dal.entities.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorEntity, Long> {

    @Query(value = "SELECT a FROM Write w JOIN w.author a WHERE w.id.bookId = :bookId")
    List<AuthorEntity> findAllByBookId(@Param("bookId") Long id);
}

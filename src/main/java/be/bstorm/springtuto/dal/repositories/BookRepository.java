package be.bstorm.springtuto.dal.repositories;

import be.bstorm.springtuto.dal.entities.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {
    @Query(value = "SELECT b FROM Book b WHERE b.active = true ORDER BY b.id, b.title")
    List<BookEntity> findAllActive();
}

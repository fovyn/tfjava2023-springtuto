package be.bstorm.springtuto.dal.entities;

import jakarta.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Entity(name = "Version")
@Table(
        name = "edition",
        uniqueConstraints = {
                @UniqueConstraint(name = "UK_Version_bookOrder", columnNames = { "book_id", "version_order" })
        }
)
public class VersionEntity extends BaseEntity {

    @Column(name="version_order", nullable = false)
    private int order;
    @Column(nullable = false)
    private float price;

    @Column(nullable = false)
    private int qtt;
    @Column(nullable = false)
    private LocalDate printDate;

    @ManyToOne(targetEntity = BookEntity.class)
    @JoinColumn(
            name = "book_id",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_Version_Book")
    )
    private BookEntity book;
}

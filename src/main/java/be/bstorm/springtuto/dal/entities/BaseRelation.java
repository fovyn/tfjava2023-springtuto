package be.bstorm.springtuto.dal.entities;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;

import java.io.Serializable;

@MappedSuperclass
@Data
public abstract class BaseRelation<TKey extends Serializable> {
    @EmbeddedId
    private TKey id;
}

package be.bstorm.springtuto.dal.entities;

import be.bstorm.springtuto.dal.entities.embedded.Address;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity(name = "BookStore")
@Table(name = "book_store")
@Data
public class BookStoreEntity extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Embedded
    private Address address;
}

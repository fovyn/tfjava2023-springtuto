package be.bstorm.springtuto.dal.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.Data;

@Entity(name = "Author")
@Table(
        name = "author",
        uniqueConstraints = {
                @UniqueConstraint(name = "UK_Author_lastname_firstname", columnNames = { "firstname", "lastname" })
        }
)
@Data
public class AuthorEntity extends BaseEntity {
    @Column(nullable = false)
    private String lastname;
    @Column(nullable = false)
    private String firstname;

    @Column(nullable = true)
    private String pseudo;
}

package be.bstorm.springtuto.dal.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity(name = "Order")
@Table(name = "order_store_version")
public class OrderRelation extends BaseRelation<OrderRelation.OrderId> {

    @Column(nullable = false)
    private int qtt;

    @ManyToOne(targetEntity = VersionEntity.class)
    @MapsId(value = "versionId")
    private VersionEntity version;

    @ManyToOne(targetEntity = BookStoreEntity.class)
    @MapsId(value = "bookStoreId")
    private BookStoreEntity bookStore;

    @Embeddable
    @Data
    public static class OrderId implements Serializable {
        private Long versionId;
        private Long bookStoreId;
    }
}

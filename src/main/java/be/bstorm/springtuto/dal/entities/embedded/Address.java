package be.bstorm.springtuto.dal.entities.embedded;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jdk.jfr.DataAmount;
import lombok.Data;

@Embeddable
@Data
public class Address {
    @Column(name = "ad_street")
    private String street;
    @Column(name = "ad_number")
    private String number;
    @Column(name = "ad_city")
    private String city;
    @Column(name = "ad_country")
    private String country;
}

package be.bstorm.springtuto.dal.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.List;

@Entity(name = "Book")
@Table(
        name = "book",
        uniqueConstraints = {
                @UniqueConstraint(name = "UK_Book_isbn", columnNames = {"isbn"})
        }
)
@Data
public class BookEntity extends BaseEntity {

    @Column(nullable = false)
    private String isbn;
    @Column(nullable = false)
    private String title;
}

package be.bstorm.springtuto.dal.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.io.Serializable;

@Entity(name = "Write")
@Table(name = "write")
@Data
public class WriteRelation extends BaseRelation<WriteRelation.WriteId> {
    @Column(nullable = false)
    private int prc;

    public float getPrc() {
        return this.prc / 100F;
    }

    @ManyToOne(targetEntity = BookEntity.class)
    @MapsId(value = "bookId")
    private BookEntity book;

    @ManyToOne(targetEntity = AuthorEntity.class)
    @MapsId(value = "authorId")
    private AuthorEntity author;

    @Embeddable
    @Data
    public static class WriteId implements Serializable {
        private Long bookId;
        private Long authorId;
    }
}
